﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy_sargento : MonoBehaviour
{

    public float health = 50;
    public Transform Objetivo;
    private NavMeshAgent agent;
    private Animator anim;
    public int distanciaN = 20;
    public float distanciaA = 1;
    private float distancia;
    private Vector3 Posicion;
    private Vector3 Direccion;
    public int Rango = 100;
    public Transform Puntodisparo;
    private bool Disparando = false;
    public Barras Vida;
    private float VelocidadDisparo = 0.0f;
 

    public void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }

    public void Update()
    {
        distancia = Vector3.Distance(transform.position, Objetivo.position);
        transform.LookAt(Objetivo);


        if (distancia >= distanciaN)
        {
            anim.SetBool("Idle", true);
            agent.speed = 0.0f;
            Disparando = false;

        }
        else if (distancia < distanciaN && distancia > distanciaA)
        {
            transform.LookAt(Objetivo);
            agent.destination = Objetivo.position;
            anim.SetBool("Idle", false);
            anim.SetBool("Walk", true);
            Disparando = false;

        }
        else if (distancia <= distanciaA)
        {
            transform.LookAt(Objetivo);
            agent.speed = 0.0f;
            anim.SetBool("Walk", false);
            Disparando = true;
            Disparo();
            VelocidadDisparo = VelocidadDisparo + 1 * Time.deltaTime;

        }
    }

    void Disparo()
    {
        RaycastHit Dsiparo;
        Posicion = Puntodisparo.position;
        Direccion = transform.TransformDirection(Vector3.forward);

        if (Disparando == true)
        {
            if (VelocidadDisparo >= 3)
            {
                VelocidadDisparo = 0; 

                if (Physics.Raycast(Puntodisparo.transform.position, Puntodisparo.transform.forward, out Dsiparo, Rango))
                {
                    Debug.Log(Dsiparo.transform.name);

                    PlayerMovement player = Dsiparo.transform.GetComponent<PlayerMovement>();
                    if (player != null)
                    {
                        Vida.ValActu -= 15;
                    }
                }
            }
        }
    }

    public void TakeDamage(float amount)
    {

        health -= amount;
        if (health <= 0f)
        {
            anim.SetBool("Die", true);
            Invoke("Die", 6);

        }

    }

    void OnCollisionEnter(Collision collision)
    {


        if (collision.gameObject.tag == "Shot")
        {
            anim.SetBool("Die", true);
            Invoke("Die", 6);

        }


    }


    void Die()
    {  
            Destroy(gameObject);
        
    }

    // Use this for initialization

}


