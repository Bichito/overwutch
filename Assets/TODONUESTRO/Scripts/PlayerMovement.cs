﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{

    private CharacterController controller;
    private Vector2 axis;

    public float speed;
    public Vector3 moveDirection;

    private float forceToGround = Physics.gravity.y;

    public AudioSource walk;
    public float jumpSpeed;

    private bool jump;

    public float gravityMagnitude = 1.0f;
    public PruebaDisparo municion;
    public BallShoot granadas;
    public RPG rpg;
    public Barras Hambre;
    public Barras Vida;

    private float segundos = 60.0f;
    private float minutos = 4.0f;
    private float ms = 0.0f;
    private bool Cradio = false;

    public Text Texsegundos;
    public Text Texminutos;
    public AudioSource Sradio;
    public Text Mradio;

    private float tiempo = 0.0f;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        Texminutos.enabled = false;
        Texsegundos.enabled = false;
        Mradio.enabled = false;
    }

    void Update()
    {

        if (Cradio == true)
        {
            Addtime();
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))

        {
            walk.Play();
            speed *= 2;
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            speed = 7;
            walk.Stop();
        }

        if (controller.isGrounded && !jump)
        {
            moveDirection.y = forceToGround;
        }
        else
        {
            jump = false;
            moveDirection.y += Physics.gravity.y * gravityMagnitude * Time.deltaTime;

        }

        Vector3 transformDirection = axis.x * transform.right + axis.y * transform.forward;

        moveDirection.x = transformDirection.x * speed;
        moveDirection.z = transformDirection.z * speed;

        controller.Move(moveDirection * Time.deltaTime);

        if (segundos <= 55.0f)
        {

            Mradio.enabled = false;

        }

    }



    void OnTriggerEnter(Collider col)
    {


        if (col.gameObject.tag == "Agua")
        {

            SceneManager.LoadScene("Lose");

        }

        if (col.tag == "municion")
        {

            municion.balas = 30;
            municion.Cargador = 30;
            granadas.Cgranadas = 6;
            granadas.granadas = 6;
            rpg.muni = 3;
            rpg.misiles = 3;

            Destroy(col.gameObject);
        }

        if (col.tag == "comida")
        {

            Hambre.ValActu = 100;
            Destroy(col.gameObject);
        }


        if (col.tag == "Botiquin")
        {

            Vida.ValActu = 100;
            Destroy(col.gameObject);
        }

        if (col.tag == "Radio")
        {
            Texminutos.enabled = true;
            Texsegundos.enabled = true;
            Mradio.enabled = true;
            Cradio = true;
            Destroy(col.gameObject);
            Sradio.Play();
        }

      
    }


    public void SetAxis(Vector2 inputAxis)
    {
        axis = inputAxis;
    }

    public void StartJump()
    {
        if (!controller.isGrounded) return;

        moveDirection.y = jumpSpeed;
        jump = true;
    }
    
    void Addtime()
    {
        if (minutos <= 0.0f)
        {
            SceneManager.LoadScene("Final");
        }

        Texminutos.text = "" + minutos;
        Texsegundos.text = ": " + segundos;

        ms++;
        if (ms >= 60.0f)
        {
            segundos --;
            ms = 0.0f; 
        }

        if (segundos <= 0)
        {
            minutos --;
            segundos = 60.0f;
        }
    }

}