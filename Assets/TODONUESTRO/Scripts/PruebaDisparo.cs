﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PruebaDisparo : MonoBehaviour
{

    public float damage = 10f;
    public float range = 100f;
    public Camera fpsCam;
    public AudioSource disparo;
    public GameObject impact;
    public ParticleSystem salida;
    public GameObject animacion;
    public int AnimacionDisparar = 1;
    public int Cargador = 6;
    public int balas;
    public Text numeroBalas;
    public ChangeGun cambioarma;

    // Use this for initialization

    public void Awake()
    {
        numeroBalas.text = "" + balas;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Disparo"))
        {
            Shoot();
        }

    }



    void Shoot()
    {
        if (cambioarma.armas1 == true)
        {
            if (Cargador > 0)
            {
                disparo.Play();
                animacion.GetComponent<Animation>()["Disparar"].speed = AnimacionDisparar;
                animacion.GetComponent<Animation>().Play("Disparar", PlayMode.StopAll);
                Cargador--;
                balas--;
                numeroBalas.text = "" + balas;
                
                salida.Play();

                RaycastHit hit;
                if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
                {
                    Debug.Log(hit.transform.name);

                    Enemy enemy = hit.transform.GetComponent<Enemy>();
                    if (enemy != null)
                    {
                        enemy.TakeDamage(damage);
                    }


                    Enemy_sargento enemys = hit.transform.GetComponent<Enemy_sargento>();
                    if (enemys != null)
                    {
                        enemys.TakeDamage(damage);
                    }

                    GameObject impactGO = Instantiate(impact, hit.point, Quaternion.LookRotation(hit.normal));
                    Destroy(impactGO, 0.2f);
                }
            }
        }
    } 
}
