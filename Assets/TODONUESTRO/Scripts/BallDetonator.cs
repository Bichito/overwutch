﻿using UnityEngine;
using System.Collections;

public class BallDetonator : MonoBehaviour
{

    float lifespan = 3.0f;
    public GameObject fireEffect;
    private BallShoot grenade;
    public AudioSource explosion;


    private int timecounter = 0;
   



    // Update is called once per frame
    void Update()
    {
        lifespan -= Time.deltaTime;

        if(lifespan <= 0)
        {
            Explode();
        }
    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Untagged")
        {
         //collision.gameObject.tag = "Untagged";       

            //collision.gameObject.tag = "Untagged";
            timecounter++;
            explosion.Play();

            GameObject fire = Instantiate(fireEffect, collision.transform.position, Quaternion.identity);
            fire.AddComponent<RemoveExplosion>();
            if (timecounter < 480)
            {
                Explode();
            }

        }


        if (collision.gameObject.tag == "Enemy_repos")
        {
            //collision.gameObject.tag = "Untagged";

            timecounter++;
            explosion.Play();
            GameObject fire = Instantiate(fireEffect, collision.transform.position, Quaternion.identity);
            fire.AddComponent<RemoveExplosion>();
            if (timecounter < 480)
            {
                Explode();
            }
         
            
        }
        

        if (collision.gameObject.tag == "Enemy")
        {
            //collision.gameObject.tag = "Untagged";
            timecounter++;
            GameObject fire = Instantiate(fireEffect, collision.transform.position, Quaternion.identity);
            fire.AddComponent<RemoveExplosion>();
            explosion.Play();

            if (timecounter < 480)
            {
                Explode();
            }
          
            
        }

     
    }

    void Explode()
    {
        Destroy(gameObject);
    }
}