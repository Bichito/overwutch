﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{

    public float health = 30;
    public Transform Objetivo;
    private NavMeshAgent agent;
    private Animator anim;
    public int distanciaN = 20;
    public float distanciaA = 1;
    private float distancia;

    public void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }

    public void Update()
    {
        distancia = Vector3.Distance (transform.position , Objetivo.position);
     
       

        if (distancia >= distanciaN)
        {
            anim.SetBool("Idle", true);
            agent.speed = 0.0f;
            anim.SetBool("Attack", false);
            gameObject.tag = "Untagged";
            transform.LookAt(Objetivo);
        }
        else if (distancia < distanciaN && distancia > distanciaA)
        {
           
            agent.destination = Objetivo.position;
            anim.SetBool("Idle", false);
            anim.SetBool("Walk", true);
            anim.SetBool("Attack", false);
            gameObject.tag = "Untagged";
            transform.LookAt(Objetivo);
        }
        else if (distancia <= distanciaA)
        {
           
            agent.speed = 0.0f;
            anim.SetBool("Walk", false);
            anim.SetBool("Attack", true);
            gameObject.tag = "Enemy";
        }
    }

    public void TakeDamage(float amount)
    {

        health -= amount;
        if (health <= 0f)
        {
            anim.SetBool("Die", true);
            Invoke("Die", 6);

        }

    }

    void OnCollisionEnter (Collision collision)
    {


        if (collision.gameObject.tag == "Shot")
        {
            anim.SetBool("Die", true);
            Invoke("Die", 6);

        }


    }


    void Die()
    {
        Destroy(gameObject);

    }

    // Use this for initialization

}


