﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{

    public GameObject creeper;
    public float timeToSpawn;
    public Transform points;

    IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeToSpawn);

            GameObject icreeper = Instantiate(creeper);
            Enemy cb = icreeper.GetComponent<Enemy>();
            cb.Objetivo = points;

        }
    }
}
