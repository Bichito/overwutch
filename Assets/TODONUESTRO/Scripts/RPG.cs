﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RPG : MonoBehaviour
{

    public GameObject bullet_prefab;
    public float bulletImpulse = 20f;
    public float hitForce;
    public float hitDamage;
    private Vector3 impactPosition;
    public LayerMask mask;
    public GameObject referencia;
    public ChangeGun cambioarma;
    public int muni = 3;
    public int misiles;
    public Text numeromisil;

    void Update()
    {

        if (cambioarma.armas3 == true)
        {

            if (Input.GetButtonDown("Disparo"))

                if (muni > 0)

                {

                    muni--;
                    misiles--;
                    numeromisil.text = "" + misiles;

                    GameObject thebullet = (GameObject)Instantiate(bullet_prefab, referencia.transform.position + referencia.transform.forward, referencia.transform.rotation);
                    thebullet.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask, QueryTriggerInteraction.Collide))
                    {
                        if (hit.rigidbody != null)
                        {
                            hit.rigidbody.AddForce(ray.direction * hitForce, ForceMode.Impulse);
                        }

                        hit.transform.gameObject.SendMessage("Damage", 1);

                        impactPosition = hit.point;

                        GameObject impact = new GameObject("Shot Impact");
                        impact.transform.position = impactPosition;
                    }
                }
            }
        }
    }
