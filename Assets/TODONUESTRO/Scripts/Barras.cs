﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum tipoBarra
{
    Vida, Hambre
}

public class Barras : MonoBehaviour
{

    public tipoBarra tipodeBarra;
    public Image barra;
    // public Text valText;
    private float ValMax = 100;
    public float ValActu = 100;
    public GameObject pocavida;
    private float tiempo = 0.0f;
    private bool atacado = false;


    // Update is called once per frame
    void Update()
    {

        // valText.text = tipodeBarra + ": " + ValActu.ToString();

        if (ValActu >= ValMax)
        {
            ValActu = ValMax;
        }

        if (ValActu <= 0)
        {
            ValActu = 0;
            SceneManager.LoadScene("Lose");
        }

        if (ValActu <= 20)

        {
            pocavida.SetActive(true);

        }

        if (ValActu >= 21)

        {
            pocavida.SetActive(false);

        }

        tiempo = tiempo + 1 * Time.deltaTime;



        switch (tipodeBarra)

        {
            case tipoBarra.Hambre:
                if (tiempo >= 50.0f)
                {
                    ValActu -= 5.0f;
                    float hambreBarra = ValActu / ValMax;
                    IntroValActual(hambreBarra);
                    tiempo = 0.0f;
                }
                break;

            case tipoBarra.Vida:
                if (atacado == true)
                {
                    ValActu -= 5;
                    float vidaBarra = ValActu / ValMax;
                    IntroValActual(vidaBarra);
                    atacado = false;
                }
                break;
        }


    }

    void OnTriggerEnter(Collider col)
    {

        if (col.tag == "Enemy")
        {

            atacado = true;

        }
    }

    void IntroValActual(float miBarra)
    {
        barra.fillAmount = miBarra;
    }

}
